River Runner Widget
==============================

This widget displays the current level of the St. Francis River at Roselle, MO.

This widget depends on my River Runner webservice available for download at https://bitbucket.org/amelungc/river-runner-webservice or use at http://service.chrisamelung.com/river-runner/index.php?format=json.