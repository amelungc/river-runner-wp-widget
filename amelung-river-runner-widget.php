<?php

/*
Plugin Name:  River Runner Widget
Description:  Widget to reveal current level for the St. Francis River at Roselle, MO.
Version:      1.0
Author:       Chris Amelung
Author URI:   https://chrisamelung.com
License:      GPL2
*/

if ( ! class_exists( 'Amelung_River_Runner_Widget' ) )  {

	class Amelung_River_Runner_Widget extends WP_Widget {

		private $level_arr;

		function __construct() {
			parent::__construct(
				'Amelung_River_Runner_Widget',
				__( 'River Level Widget', 'text_domain' ),
				array( 'description' => __( 'Widget to show current level of the St. Francis river at Roselle.', 'text_domain' ), )
			);

			$json_str = file_get_contents('http://service.chrisamelung.com/river-runner/index.php?format=json');
			$this->level_arr = json_decode($json_str, true);

			error_log( 'CONSTRUCTING WIDGET' );
		}

		public function widget( $args, $instance ) {
			echo $args['before_widget'];
			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args[ 'after_title' ];
			}
			echo '<p>Current level: ';
			if ( $this->level_arr["gauge_level"] > 3.0 ) {
				echo $this->level_arr["converted_level"] . ' ' . $this->level_arr["converted_units"];
			} else {
				echo $this->level_arr["conditions"];
			}
			echo '</p>';
			echo $args['after_widget'];
		}

		public function form( $instance ) {
			if ( isset( $instance['title'] ) ) {
				$title = $instance['title'];
			} else {
				$title = __( 'New title', 'wpb_widget_domain' );
			}
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ) ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<?php
		}

		public function update( $new_instance, $old_instance ) {
			$instance = array();
			$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

			return $instance;
		}

	} // end Amelung_River_Runner_Widget

	// Register and load the widget
	function amelung_rr_load_widget() {
		register_widget( 'Amelung_River_Runner_Widget' );
	}
	add_action( 'widgets_init', 'amelung_rr_load_widget' );

}